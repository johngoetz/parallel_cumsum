#include <cstdlib>
#include <execution>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

#include <valgrind/callgrind.h>

#include "containerio.hpp"
#include "cumsum.hpp"
#include "timeit.hpp"

using namespace std;
using namespace test;

template <typename T>
vector<T> generate(size_t n)
{
    random_device rdev{};
    mt19937 gen{rdev()};
    uniform_int_distribution<> dist{0, 1024 * 1024};
    vector<T> result(n);
    for (size_t i = 0; i < n; ++i)
    {
        result[i] = dist(gen);
    }
    return result;
}

int main(int argc, char** argv)
{
    using source_index_t = long int;
    using index_t = long long int;
    int n{20000000};
    if (argc > 1)
    {
        n = atoi(argv[1]);
    }
    clog << "n: " << n << endl;
    clog << "jobs: " << std::max(1u, std::thread::hardware_concurrency() / 2) << endl;
    auto data = generate<source_index_t>(n);
    clog << data << endl;
    unsigned long long int data_sum{std::reduce(data.begin(), data.end(), 0ull)};
    if (data_sum > std::numeric_limits<source_index_t>::max())
    {
        clog << "WARNING: sum will be larger than SOURCE type can hold.\n";
    }
    if (data_sum > std::numeric_limits<index_t>::max())
    {
        clog << "WARNING: sum will be larger than TARGET type can hold.\n";
    }

    //CALLGRIND_START_INSTRUMENTATION;

    //{
    //    index_t total;
    //    auto datacopy{data};
    //    vector<index_t> datacopy_cumsum(datacopy.size());
    //    { TimeIt timeit{"serial cumsum copy"};
    //        total = cumsum_serial(datacopy, datacopy_cumsum);
    //    } /// TIMEIT
    //    clog << "    " << datacopy_cumsum << endl;
    //    clog << "    total: " << total << endl;
    //}
    {
        index_t total;
        auto datacopy{data};
        {
            TimeIt timeit{"serial cumsum in-place"};
            //CALLGRIND_TOGGLE_COLLECT;
            total = cumsum_serial(datacopy);
            //CALLGRIND_TOGGLE_COLLECT;
        } /// TIMEIT
        clog << "    " << datacopy << endl;
        clog << "    total: " << total << endl;
    }
    //{
    //    index_t total;
    //    auto datacopy{data};
    //    vector<index_t> datacopy_cumsum(datacopy.size());
    //    { TimeIt timeit{"parallel cumsum copy"};
    //        total = cumsum(datacopy, datacopy_cumsum);
    //    } /// TIMEIT
    //    clog << "    " << datacopy_cumsum << endl;
    //    clog << "    total: " << total << endl;
    //}
    {
        index_t total;
        auto datacopy{data};
        {
            TimeIt timeit{"parallel cumsum in-place"};
            //CALLGRIND_TOGGLE_COLLECT;
            total = cumsum(datacopy);
            //CALLGRIND_TOGGLE_COLLECT;
        } /// TIMEIT
        clog << "    " << datacopy << endl;
        clog << "    total: " << total << endl;
    }

    //CALLGRIND_STOP_INSTRUMENTATION;
    //CALLGRIND_DUMP_STATS;
}
