#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <future>
#include <numeric>
#include <span>
#include <thread>
#include <vector>
#include <queue>

#ifndef MODE
    #define MODE 0
#endif

// Use (void) to silence unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

template <
    class InIt,
    class OutIt,
    typename OutType = OutIt::value_type
    >
OutType cumsum_serial(InIt first, InIt const last, OutIt d_first)
{
    assertm(first <= last, "Input iterators must be in order.");
    if (std::distance(first, last) > 0)
    {
        switch (MODE)
        {
            case 0:
            default:   /// using standard library C++20
            {
                return *--std::inclusive_scan(
                    std::execution::seq,
                    first, last, d_first,
                    std::plus<OutType>(),
                    OutType{0});
            }
            case 1:   /// algorithm using C++17 transform
            {
                OutType sum = 0;
                auto itr = std::transform(first, last, d_first,
                    [&sum] (OutType item) {
                        sum += item;
                        return sum;
                    }
                );
                return *--itr;
            }
            case 2:   /// algorithm spelled-out
            {
                OutType sum = 0;
                while (first != last)
                {
                    sum += *first++;
                    *d_first++ = sum;
                }
                return sum;
            }
            case 3:   /// "expensive" operation, same result
            {
                OutType sum{0};
                while (first != last)
                {
                    sum += std::exp(std::log(*first++));
                    *d_first++ = sum;
                }
                return sum;
            }
        }
    }
    else
    {
        return 0;
    }
}

template <typename InType, typename OutType>
OutType cumsum_serial(InType const* first, InType const* const last, OutType* d_first)
{
    return cumsum_serial<InType const*, OutType*, OutType>(first, last, d_first);
}

template <typename ValueType>
ValueType cumsum_serial(std::vector<ValueType>& v)
{
    return cumsum_serial(v.begin(), v.end(), v.begin());
}

template <typename InType, typename OutType>
OutType cumsum_serial(std::vector<InType> const& in, std::vector<OutType>& out)
{
    return cumsum_serial(in.begin(), in.end(), out.begin());
}

template <
    class InIt,
    class OutIt,
    typename InType = InIt::value_type,
    typename OutType = OutIt::value_type,
    typename ValueType
    >
OutType add(InIt first, InIt const last, OutIt d_first, ValueType const value)
{
    assertm(first <= last, "Input iterators must be in order.");
    if (std::distance(first, last) > 0)
    {
        return *--std::transform(
            first, last, d_first,
            [value] (InType item)
            {
                return item + value;
            }
        );
    }
    else
    {
        return 0;
    }
}

template <typename InType, typename OutType, typename ValueType>
OutType add(
    InType const* first,
    InType const* const last,
    OutType* d_first,
    ValueType const value
)
{
    return add<
        InType const*,
        OutType*, InType,
        OutType,
        ValueType
        >(first, last, d_first, value);
}

template <
    class InIt,
    class OutIt,
    typename InType = InIt::value_type,
    typename OutType = OutIt::value_type
    >
OutType cumsum(InIt first, InIt const last, OutIt d_first)
{
    constexpr std::size_t chunk_size_threshold = 524288;
    std::size_t const jobs = std::max(1u, std::thread::hardware_concurrency() / 2);
    assertm(first <= last, "Input iterators must be in order.");
    std::size_t const size = static_cast<std::size_t>(std::distance(first, last));
    if (jobs > 1 && (size / jobs) > chunk_size_threshold)
    {
        std::vector<std::span<InType const>> inspans{};
        std::vector<std::span<OutType>> outspans{};
        std::size_t const q = size / jobs;
        std::size_t const r = size % jobs;
        for (std::size_t i = 0; i < jobs; ++i)
        {
            std::size_t const offset = i * q + std::min(i, r);
            std::size_t const end_offset = (i + 1) * q + std::min(i + 1, r);
            inspans.emplace_back(first + offset, first + end_offset);
            outspans.emplace_back(d_first + offset, d_first + end_offset);
        }
        std::vector<OutType> spansums{};
        std::queue<std::thread> pool{};
        std::queue<std::future<OutType>> futures{};
        for (std::size_t i = 0; i < jobs; ++i)
        {
            std::packaged_task<OutType(void)> task
            {
                [&inspan = inspans[i], &outspan = outspans[i]]
                {
                    return cumsum_serial(inspan.begin(), inspan.end(), outspan.begin());
                }
            };
            futures.emplace(task.get_future());
            pool.emplace(std::move(task));
        }
        while (!pool.empty())
        {
            pool.front().join();
            spansums.emplace_back(futures.front().get());
            futures.pop();
            pool.pop();
        }
        OutType total = cumsum_serial(spansums);
        for (std::size_t i = 1; i < jobs; ++i)
        {
            pool.emplace(
                [&outspan = outspans[i], value = spansums[i - 1]]
                {
                    return add(outspan.begin(), outspan.end(), outspan.begin(), value);
                }
            );
        }
        while (!pool.empty())
        {
            pool.front().join();
            pool.pop();
        }
        return total;
    }
    else
    {
        return cumsum_serial(first, last, d_first);
    }
}

template <typename InType, typename OutType>
OutType cumsum(InType const* first, InType const* const last, OutType* d_first)
{
    return cumsum<InType const*, OutType*, InType, OutType>(first, last, d_first);
}

template <typename ValueType>
ValueType cumsum(std::vector<ValueType>& v)
{
    return cumsum(v.begin(), v.end(), v.begin());
}

template <typename InType, typename OutType>
OutType cumsum(std::vector<InType> const& in, std::vector<OutType>& out)
{
    return cumsum(in.begin(), in.end(), out.begin());
}
