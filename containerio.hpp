#pragma once

#include <cstddef>
#include <ostream>
#include <vector>

template <typename T>
std::ostream& operator<<(std::ostream& os, std::vector<T>& v)
{
    std::size_t n = std::min(std::size_t{5}, v.size());
    os << "(";
    for (std::size_t i = 0; i < n; ++i)
    {
        if (i > 0)
        {
            os << ", ";
        }
        os << v[i];
    }
    if (v.size() > 10)
    {
        os << " ... ";
        std::size_t start = v.size() - 5;
        for (std::size_t i = start; i < v.size(); ++i)
        {
            if (i > start)
            {
                os << ", ";
            }
            os << v[i];
        }
    }
    else if (v.size() > 5)
    {
        for (std::size_t i = 5; i < v.size(); ++i)
        {
            os << ", " << v[i];
        }
    }
    os << ")";
    return os;
}
