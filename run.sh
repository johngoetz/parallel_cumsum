# usage:
#   ./run.sh [MODE] [SIZE]
# where MODE is 0, 1 or 2 (default: 0)
# and SIZE is the number of elements in the vector
# (default: 20000000)
if [[ $# -gt 0 ]]; then MODE=$1; else MODE=0; fi
if [[ $# -gt 1 ]]; then SIZE=$2; else SIZE=20000000; fi
g++ -Wall -std=c++20 -Ofast test.cpp -DMODE=$MODE && ./a.out $SIZE
