#pragma once

#include <chrono>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ratio>

namespace test {

namespace fs = std::filesystem;
namespace cr = std::chrono;

class Timer
{
    using wall_clock = cr::high_resolution_clock;
    using milliseconds = cr::duration<double, std::milli>;

  public:
    Timer(std::string name, fs::path output = fs::path{"-"})
        : m_name{name}
        , m_output{output}
        , m_procTime{0}
        , m_wallTime{0}
        , m_procStart{}
        , m_wallStart{}
    {}

    void start()
    {
        m_procStart = std::clock();
        m_wallStart = wall_clock::now();
    }

    void stop()
    {
        auto procEnd = std::clock();
        auto wallEnd = wall_clock::now();
        milliseconds procTime{(procEnd - m_procStart)* (1000. / CLOCKS_PER_SEC)};
        milliseconds wallTime{wallEnd - m_wallStart};
        m_procTime += procTime;
        m_wallTime += wallTime;
    }

    void write_output()
    {
        std::stringstream ss;
        ss << std::setprecision(6);
        ss << "- name: " << m_name << "\n"
           << "  proc: " << m_procTime.count() << "\n"
           << "  wall: " << m_wallTime.count() << "\n";
        if (m_output == fs::path("-"))
        {
            std::cout << ss.str();
        }
        else
        {
            std::ofstream fout(m_output, std::ios::app);
            fout << ss.str();
        }
    }

    void reset()
    {
        m_procTime = milliseconds{0};
        m_wallTime = milliseconds{0};
    }

  private:
    std::string m_name;
    fs::path m_output;
    milliseconds m_procTime;
    milliseconds m_wallTime;
    std::clock_t m_procStart;
    cr::time_point<wall_clock> m_wallStart;
};

class TimeIt
{
  public:
    TimeIt(std::string name, fs::path output = fs::path{"-"})
        : m_timer{name, output}
    {
        m_timer.start();
    }

    ~TimeIt()
    {
        m_timer.stop();
        m_timer.write_output();
    }

  private:
    Timer m_timer;
};
}
